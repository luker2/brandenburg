# brandenburg
For security reasons many organisations need to gate access to systems containing sensitive or personal data. Best practice usually requires:
* Access request and approval
* Regular user access reviews

brandenburg is a tool that supports that process using Slack

## Functional objectives
* Allow a user to easily request access via Slack
* Allow approvers to approve via Slack and allow application owners to confirm application access has been provisioned via Slack
* Provide an audit trail of access requests, required approvals and access completion
* Support periodic user access reviews and provide an audit trail for those 

## Non functional objectives
* (Personally) learn golang and try to apply many of the recommended best practices
* Loosely couple Slack so that someone could implement a different UI that users could interact with
* Allow other systems to bact on the events happening within brandenburg (e.g. via subscribing to webhooks)

## Screenshots
![Select application](screenshots/selectApplication.png "Select application")
![Confirm request](screenshots/confirmRequest.png "Confirm request")
![Approval request](screenshots/requestApproval.png "Request Approval")

## Design
There are 3 executables / components to brandenburg:
* **api** This is a REST style HTTP service backed by mongodb that stores the audit trail of access requests, approvals and access changes. It also can send outbound POST requests to any number of notification targets (a basic form of webhooks)
* **slack** This is an HTTP service that receives [POST requests from Slack](https://api.slack.com/messaging/interactivity#components) and also POST requests from the api service. This slack component could be replaced, or added to, to support interaction via other UIs (e.g. email, web UI)
* **loader** is a cli tool that can import data into the db from .json files

## Getting started
* Clone this repo using `git clone git@gitlab.com:bradleyscott/brandenburg.git`
* Define what applications brandenburg will allow access requests to by editing the file at `cmd/loader/applications.example.json`, renaming the file appropriately
* The Slack UI ([modal and message templates](https://api.slack.com/surfaces)) are editable. Have a look at the templates at `cmd/slack/example-templates` and when you are happy rename the folder to `cmd/slack/templates`
* [Set up your Slack app](https://api.slack.com/start), [define a shortcut](https://api.slack.com/actions) with the callback-id `select-application` and set a request Url that will reach your development environment
* Grab your Slack app's `Bot User OAuth Access Token`. This will start with `xoxb-`
* Start the db, API and slack components using `SLACK_TOKEN=[your token] docker-compose up`
 * Load your applications into the db using the loader executable: `./cmd/loader/go run loader.go applications --file [your-applications.json]`
 * Use your Slack app's shortcut to start playing with brandenburg