# brandenburg API
This executable starts a REST-style HTTP API. The resources are:
* `/applications` supporting GET all, GET by id, and POST
* `/accessRequests` supporting GET by id, and POST

Approvals and Access changes can be recorded against an access request by POSTing to `/accessRequests/{id}/approval` or `/accessRequests/{id}/accessChange` respectively

## Environment variables
| Variable key | Description | Default
|---|---|---|
| LOG_LEVEL | Determines the verbosity of logging | Info
| API_PORT | The port the api service listens on | 3000
| DB_HOST | Hostname of mongodb | db
| DB_PORT | mongodb PORT | 27017
| DB_USER | User to authenticate to mongodb with | -
| DB_PASSWORD | Password of the user specified in DB_USER | -
| DB_NAME | The database that will contain brandenburg data | brandenburg
| NOTIFY_TARGETS | A comma separated list of urls that will receive HTTP POST requests when Access requests are created, when Approvals are received or Access changes are made | -