package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"

	figure "github.com/common-nighthawk/go-figure"
	"github.com/joho/godotenv"

	"github.com/urfave/cli/v2"
	"gitlab.com/bradleyscott/brandenburg/internal/utils"
	"gitlab.com/bradleyscott/brandenburg/pkg/apiclient"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

var (
	filename  string
	apiClient apiclient.APIClient
	err       error
)

func main() {
	godotenv.Load(".env") // Load .env file
	utils.ConfigureLogging()

	host := os.Getenv("API_URL")
	if host == "" {
		host = "http://localhost:3000"
	}

	apiClient = apiclient.NewAPIClient(host)
	if err != nil {
		log.Panic("Unable to connect to DB")
	}

	app := &cli.App{}
	app.EnableBashCompletion = true
	app.Name = "brandenburg DB loader"
	app.Usage = "Hydrates DB with records from files"
	app.Commands = []*cli.Command{
		{
			Name:    "applications",
			Aliases: []string{"apps"},
			Usage:   "load Application records from file",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "file",
					Aliases:     []string{"f"},
					Value:       "applications.json",
					Usage:       "filename of json file containing applications to load",
					Destination: &filename,
					EnvVars:     []string{"APPLICATIONS_FILE"},
				},
			},
			Action: loadApplications,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func loadApplications(c *cli.Context) error {
	figure.NewFigure("brandenburg", "gothic", true).Print()

	filename = c.String("file")
	log.Debugf("Attempting to read Applications list from %s", filename)

	applications := make([]types.Application, 0)
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("A problem occurred reading the file at %s. %+v", filename, err)
	}

	err = json.Unmarshal([]byte(file), &applications)
	if err != nil {
		log.Fatalf("A problem occurred attempting to parse Application list from %s. %+v", filename, err)
	}

	log.Debugf("Successfully read %d Applications from %s \n", len(applications), filename)
	for _, a := range applications {
		_, err := apiClient.CreateApplication(&a)
		if err != nil {
			log.Fatalf("A problem occurred attempting to load Application named %s", a.Name)
		}

		log.Debugf("Succesfully loaded Application named %s", a.Name)
	}

	log.Infof("Successfully loaded all %d Applications from %s", len(applications), filename)

	fmt.Printf("Done. Auf Wiedersehen!")
	return nil
}
