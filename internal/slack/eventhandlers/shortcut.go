package eventhandlers

import (
	"encoding/json"
	"errors"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/payloads"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// ShortcutEventHandler can handle shortcut payloads from Slack
type ShortcutEventHandler struct {
	Renderer    shared.TemplateRenderer
	APIClient   types.BrandenburgClient
	SlackClient shared.Slacker
}

// NewShortcutEventHandler initialises a new handler based on provided options
func NewShortcutEventHandler(options shared.HandlerOptions) ShortcutEventHandler {
	handler := ShortcutEventHandler{
		Renderer:    options.Renderer,
		APIClient:   options.APIClient,
		SlackClient: options.SlackClient,
	}
	return handler
}

// ProcessPayload processes shortcut payloads
func (h ShortcutEventHandler) ProcessPayload(payload string) (string, error) {

	var incomingPayload payloads.ShortcutPayload
	err := json.Unmarshal([]byte(payload), &incomingPayload)
	if err != nil {
		log.WithField("err", err.Error()).Error("Unable to parse shortcut payload")
		return "", err
	}

	switch incomingPayload.CallbackID {
	case "select-application":
		log.Debug("select-application ShortcutEvent received")
		view, err := h.renderSelectApplicationTemplate(incomingPayload)
		err = h.SlackClient.OpenView(view)

		if err != nil {
			return "", err
		}
		return "", nil
	default:
		message := "No handler exists for this shortcut type"
		log.WithField("callbackID", incomingPayload.CallbackID).Warn(message)
		return "", errors.New(message)
	}
}

func (h ShortcutEventHandler) renderSelectApplicationTemplate(payload payloads.ShortcutPayload) (string, error) {
	applicationOptions, err := h.renderApplicationOptionTemplate()
	type templateContext struct {
		TriggerID    string
		Applications string
	}
	context := templateContext{
		TriggerID:    payload.TriggerID,
		Applications: applicationOptions,
	}

	var reply string
	reply, err = h.Renderer.RenderTemplate("selectApplication", context)
	return reply, err
}

func (h ShortcutEventHandler) renderApplicationOptionTemplate() (string, error) {
	context := ""
	applications, err := h.APIClient.GetApplications()
	if err != nil {
		log.WithField("err", err.Error()).Warn("A problem occurred trying to retrieve Applications")
	}

	for _, a := range applications {
		newContext, _ := h.Renderer.RenderTemplate("applicationOption", a)
		context += newContext
	}
	context = strings.TrimRight(context, ",")
	return context, nil
}
