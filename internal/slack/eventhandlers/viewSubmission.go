package eventhandlers

import (
	"encoding/json"
	"errors"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/payloads"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// ViewSubmissionEventHandler can handle view_submission paylods from Slack
type ViewSubmissionEventHandler struct {
	Renderer    shared.TemplateRenderer
	APIClient   types.BrandenburgClient
	SlackClient shared.Slacker
}

// NewViewSubmissionEventHandler initialises a new handler based on provided options
func NewViewSubmissionEventHandler(options shared.HandlerOptions) ViewSubmissionEventHandler {
	handler := ViewSubmissionEventHandler{
		Renderer:    options.Renderer,
		APIClient:   options.APIClient,
		SlackClient: options.SlackClient,
	}
	return handler
}

// ProcessPayload processes view_submission payloads
func (h ViewSubmissionEventHandler) ProcessPayload(payload string) (string, error) {

	var incomingPayload payloads.ViewSubmissionPayload
	err := json.Unmarshal([]byte(payload), &incomingPayload)
	if err != nil {
		log.WithField("err", err.Error()).Error("Unable to parse view_submission payload")
		return "", err
	}

	switch incomingPayload.View.CallbackID {
	case "access-agreement":
		log.Debug("access-agreement ViewSubmission event received")

		applicationID := incomingPayload.View.State.Values.ApplicationBlock.ApplicationSelect.SelectedOption.Value
		application, err := h.APIClient.GetApplication(applicationID)
		if err != nil {
			return "", err
		}

		if application.RequiresAccessAgreement {
			return h.getAccessAgreement(application)
		}

		incomingPayload.View.PrivateMetadata = applicationID
		var accessRequest *types.AccessRequest
		accessRequest, err = h.makeAccessRequest(incomingPayload, application)
		if err != nil {
			return "", err
		}

		var responseJSON string
		responseJSON, err = h.renderConfirmRequestTemplate(accessRequest)
		if err != nil {
			return "", err
		}
		return responseJSON, err

	case "finish-request":
		log.Debug("finish-request ViewSubmission event received")

		applicationID := incomingPayload.View.PrivateMetadata
		application, err := h.APIClient.GetApplication(applicationID)
		if err != nil {
			return "", err
		}

		var accessRequest *types.AccessRequest
		accessRequest, err = h.makeAccessRequest(incomingPayload, application)
		if err != nil {
			return "", err
		}

		var responseJSON string
		responseJSON, err = h.renderConfirmRequestTemplate(accessRequest)
		if err != nil {
			return "", err
		}
		return responseJSON, err

	default:
		message := "No handler exists for this view_submission type"
		log.WithField("callbackID", incomingPayload.View.CallbackID).Warn(message)
		return "", errors.New(message)
	}
}

func (h ViewSubmissionEventHandler) getAccessAgreement(application *types.Application) (string, error) {
	type templateContext struct {
		Application *types.Application
	}
	context := &templateContext{
		Application: application,
	}

	reply, _ := h.Renderer.RenderTemplate("accessAgreement", context)
	return reply, nil
}

func (h ViewSubmissionEventHandler) renderConfirmRequestTemplate(accessRequest *types.AccessRequest) (string, error) {
	type templateContext struct {
		AccessRequest    *types.AccessRequest
		ApprovalRequired bool
		Approvers        []string
		Owners           []string
	}

	approvers, _ := h.SlackClient.GetUsernames(accessRequest.Application.Approvers)
	owners, _ := h.SlackClient.GetUsernames(accessRequest.Application.Owners)

	context := &templateContext{
		AccessRequest:    accessRequest,
		ApprovalRequired: accessRequest.IsApprovalRequired(),
		Approvers:        approvers,
		Owners:           owners,
	}

	reply, _ := h.Renderer.RenderTemplate("confirmRequest", context)
	return reply, nil
}

func (h ViewSubmissionEventHandler) makeAccessRequest(payload payloads.ViewSubmissionPayload, application *types.Application) (*types.AccessRequest, error) {

	requestor, _ := h.SlackClient.GetEmail(payload.User.ID)
	accessRequest := &types.AccessRequest{
		Application: application,
		RequestedAt: time.Now(),
		Requestor:   requestor,
	}

	if application.RequiresAccessAgreement {
		var timeNow time.Time
		timeNow = time.Now()
		accessRequest.AccessAgreementAt = &timeNow
	}

	return h.APIClient.CreateAccessRequest(accessRequest)
}
