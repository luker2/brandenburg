package notificationhandlers

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// AccessChangedNotificationHandler can handle Approval* notifications
type AccessChangedNotificationHandler struct {
	Renderer    shared.TemplateRenderer
	APIClient   types.BrandenburgClient
	SlackClient shared.Slacker
}

// NewAccessChangedNotificationHandler initialises a new handler based on provided options
func NewAccessChangedNotificationHandler(options shared.HandlerOptions) AccessChangedNotificationHandler {
	handler := AccessChangedNotificationHandler{
		Renderer:    options.Renderer,
		APIClient:   options.APIClient,
		SlackClient: options.SlackClient,
	}
	return handler
}

// ProcessNotification processes AccessChanged notifications and informs requestors
func (h AccessChangedNotificationHandler) ProcessNotification(notification *types.Notification) (string, error) {
	log.Debug("AccessChanged notification received")

	go h.notifyRequestor(notification.AccessRequest, notification.AccessChange)
	return "", nil
}

func (h AccessChangedNotificationHandler) notifyRequestor(accessRequest *types.AccessRequest, accessChange *types.AccessChange) error {
	channel, err := h.SlackClient.OpenConversation([]string{accessRequest.Requestor})
	if err != nil {
		return err
	}

	var blocks string
	blocks, err = h.renderConfirmAccessChangeTemplate(accessRequest, accessChange)
	if err != nil {
		return err
	}

	var message string
	if accessChange.Type == types.AccessChangeTypes.Granted {
		message = fmt.Sprintf("Your %s access has been granted", accessRequest.Application.Name)
	} else if accessChange.Type == types.NotificationTypes.AccessChanged {
		message = fmt.Sprintf("Your %s access has been declined", accessRequest.Application.Name)
	}

	err = h.SlackClient.PostMessage(channel, blocks, message)
	if err != nil {
		return err
	}

	return nil
}

func (h AccessChangedNotificationHandler) renderConfirmAccessChangeTemplate(accessRequest *types.AccessRequest, accessChange *types.AccessChange) (string, error) {
	type templateContext struct {
		Requestor     string
		Owner         string
		AccessRequest *types.AccessRequest
		AccessChange  *types.AccessChange
		IsGranted     bool
	}

	owner, _ := h.SlackClient.GetUsername(accessChange.Owner)
	requestor, _ := h.SlackClient.GetUsername(accessRequest.Requestor)
	isGranted := (accessChange.Type == types.AccessChangeTypes.Granted)

	context := templateContext{
		Requestor:     requestor,
		Owner:         owner,
		AccessRequest: accessRequest,
		AccessChange:  accessChange,
		IsGranted:     isGranted,
	}

	return h.Renderer.RenderTemplate("confirmAccessChange", context)
}
