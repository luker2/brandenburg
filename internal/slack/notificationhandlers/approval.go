package notificationhandlers

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// ApprovalReceivedNotificationHandler can handle Approval* notifications
type ApprovalReceivedNotificationHandler struct {
	Renderer    shared.TemplateRenderer
	APIClient   types.BrandenburgClient
	SlackClient shared.Slacker
}

// NewApprovalReceivedNotificationHandler initialises a new handler based on provided options
func NewApprovalReceivedNotificationHandler(options shared.HandlerOptions) ApprovalReceivedNotificationHandler {
	handler := ApprovalReceivedNotificationHandler{
		Renderer:    options.Renderer,
		APIClient:   options.APIClient,
		SlackClient: options.SlackClient,
	}
	return handler
}

// ProcessNotification processes ApprovalReceived notifications, updates requestors and asks for access to be provisioned
func (h ApprovalReceivedNotificationHandler) ProcessNotification(notification *types.Notification) (string, error) {
	log.Info("ApprovalReceived notification received")

	go h.requestAccessChange(notification.AccessRequest, notification.Approval)
	go h.notifyRequestor(notification.AccessRequest, notification.Approval)
	return "", nil
}

func (h ApprovalReceivedNotificationHandler) requestAccessChange(accessRequest *types.AccessRequest, approval *types.Approval) error {
	channel, err := h.SlackClient.OpenConversation(accessRequest.Application.Owners)
	if err != nil {
		return err
	}

	var blocks string
	blocks, err = h.renderRequestAccessChangeTemplate(accessRequest, approval)
	if err != nil {
		return err
	}

	message := fmt.Sprintf("An access request for %s requires your attention", accessRequest.Application.Name)
	err = h.SlackClient.PostMessage(channel, blocks, message)
	return err
}

func (h ApprovalReceivedNotificationHandler) notifyRequestor(accessRequest *types.AccessRequest, approval *types.Approval) error {
	channel, err := h.SlackClient.OpenConversation([]string{accessRequest.Requestor})
	if err != nil {
		return err
	}

	var blocks string
	blocks, err = h.renderConfirmApprovalTemplate(accessRequest, approval)
	if err != nil {
		return err
	}

	var message string
	if approval.Type == types.ApprovalTypes.Approved {
		message = fmt.Sprintf("Your %s access request has been approved", accessRequest.Application.Name)
	} else if approval.Type == types.ApprovalTypes.Declined {
		message = fmt.Sprintf("Your %s acess request has been declined", accessRequest.Application.Name)
	}

	err = h.SlackClient.PostMessage(channel, blocks, message)
	if err != nil {
		return err
	}

	return nil
}

func (h ApprovalReceivedNotificationHandler) renderRequestAccessChangeTemplate(accessRequest *types.AccessRequest, approval *types.Approval) (string, error) {
	type templateContext struct {
		Requestor       string
		RequestApprover string
		AccessRequest   *types.AccessRequest
		Approval        *types.Approval
	}

	requestor, _ := h.SlackClient.GetUsername(accessRequest.Requestor)
	approver, _ := h.SlackClient.GetUsername(approval.Approver)
	context := templateContext{
		Requestor:       requestor,
		RequestApprover: approver,
		AccessRequest:   accessRequest,
		Approval:        approval,
	}

	return h.Renderer.RenderTemplate("requestAccessChange", context)
}

func (h ApprovalReceivedNotificationHandler) renderConfirmApprovalTemplate(accessRequest *types.AccessRequest, approval *types.Approval) (string, error) {
	type templateContext struct {
		Requestor     string
		Approver      string
		AccessRequest *types.AccessRequest
		Approval      *types.Approval
		IsApproved    bool
		Owners        []string
	}

	owners, _ := h.SlackClient.GetUsernames(accessRequest.Application.Owners)
	requestor, _ := h.SlackClient.GetUsername(accessRequest.Requestor)
	approver, _ := h.SlackClient.GetUsername(approval.Approver)
	isApproved := (approval.Type == types.ApprovalTypes.Approved)

	context := templateContext{
		Requestor:     requestor,
		Approver:      approver,
		AccessRequest: accessRequest,
		Approval:      approval,
		IsApproved:    isApproved,
		Owners:        owners,
	}

	return h.Renderer.RenderTemplate("confirmApproval", context)
}
