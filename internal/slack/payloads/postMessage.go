package payloads

// PostMessageRequest is the payload sent to Slack during the chat.postMessage request
type PostMessageRequest struct {
	Text    string `json:"text"`
	Channel string `json:"channel"`
	Blocks  string `json:"blocks"`
}

// PostMessageResponse is returned by Slack after a chat.postMessage request
type PostMessageResponse struct {
	Ok      bool   `json:"ok"`
	Channel string `json:"channel"`
	Ts      string `json:"ts"`
	Message struct {
		Text        string `json:"text"`
		Username    string `json:"username"`
		BotID       string `json:"bot_id"`
		Attachments []struct {
			Text     string `json:"text"`
			ID       int    `json:"id"`
			Fallback string `json:"fallback"`
		} `json:"attachments"`
		Type    string `json:"type"`
		Subtype string `json:"subtype"`
		Ts      string `json:"ts"`
	} `json:"message"`
}
