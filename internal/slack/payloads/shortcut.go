package payloads

// ShortcutPayload describes the payload received from Slack when a user invokes a global shortcut
type ShortcutPayload struct {
	Type     string `json:"type"`
	Token    string `json:"token"`
	ActionTs string `json:"action_ts"`
	Team     struct {
		ID     string `json:"id"`
		Domain string `json:"domain"`
	} `json:"team"`
	User struct {
		ID       string `json:"id"`
		Username string `json:"username"`
		TeamID   string `json:"team_id"`
	} `json:"user"`
	CallbackID string `json:"callback_id"`
	TriggerID  string `json:"trigger_id"`
}
