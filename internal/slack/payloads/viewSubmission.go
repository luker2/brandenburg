package payloads

// ViewSubmissionPayload describes the payload received from Slack when a user clicks a dialog button
type ViewSubmissionPayload struct {
	Type string `json:"type"`
	Team struct {
		ID     string `json:"id"`
		Domain string `json:"domain"`
	} `json:"team"`
	User struct {
		ID       string `json:"id"`
		Username string `json:"username"`
		Name     string `json:"name"`
		TeamID   string `json:"team_id"`
	} `json:"user"`
	APIAppID  string `json:"api_app_id"`
	Token     string `json:"token"`
	TriggerID string `json:"trigger_id"`
	View      struct {
		ID     string `json:"id"`
		TeamID string `json:"team_id"`
		Type   string `json:"type"`
		Blocks []struct {
			Type    string `json:"type"`
			BlockID string `json:"block_id"`
			Text    struct {
				Type     string `json:"type"`
				Text     string `json:"text"`
				Verbatim bool   `json:"verbatim"`
			} `json:"text,omitempty"`
			Label struct {
				Type  string `json:"type"`
				Text  string `json:"text"`
				Emoji bool   `json:"emoji"`
			} `json:"label,omitempty"`
			Optional bool `json:"optional,omitempty"`
			Element  struct {
				Type        string `json:"type"`
				ActionID    string `json:"action_id"`
				Placeholder struct {
					Type  string `json:"type"`
					Text  string `json:"text"`
					Emoji bool   `json:"emoji"`
				} `json:"placeholder"`
				Options []struct {
					Text struct {
						Type  string `json:"type"`
						Text  string `json:"text"`
						Emoji bool   `json:"emoji"`
					} `json:"text"`
					Value string `json:"value"`
				} `json:"options"`
			} `json:"element,omitempty"`
		} `json:"blocks"`
		PrivateMetadata string `json:"private_metadata"`
		CallbackID      string `json:"callback_id"`
		State           struct {
			Values struct {
				ApplicationBlock struct {
					ApplicationSelect struct {
						Type           string `json:"type"`
						SelectedOption struct {
							Text struct {
								Type  string `json:"type"`
								Text  string `json:"text"`
								Emoji bool   `json:"emoji"`
							} `json:"text"`
							Value string `json:"value"`
						} `json:"selected_option"`
					} `json:"application_select"`
				} `json:"application_block"`
			} `json:"values"`
		} `json:"state"`
		Hash  string `json:"hash"`
		Title struct {
			Type  string `json:"type"`
			Text  string `json:"text"`
			Emoji bool   `json:"emoji"`
		} `json:"title"`
		ClearOnClose  bool `json:"clear_on_close"`
		NotifyOnClose bool `json:"notify_on_close"`
		Close         struct {
			Type  string `json:"type"`
			Text  string `json:"text"`
			Emoji bool   `json:"emoji"`
		} `json:"close"`
		Submit struct {
			Type  string `json:"type"`
			Text  string `json:"text"`
			Emoji bool   `json:"emoji"`
		} `json:"submit"`
		PreviousViewID     interface{} `json:"previous_view_id"`
		RootViewID         string      `json:"root_view_id"`
		AppID              string      `json:"app_id"`
		ExternalID         string      `json:"external_id"`
		AppInstalledTeamID string      `json:"app_installed_team_id"`
		BotID              string      `json:"bot_id"`
	} `json:"view"`
	ResponseUrls []interface{} `json:"response_urls"`
}
