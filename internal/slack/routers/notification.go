package routers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/shared"
	"gitlab.com/bradleyscott/brandenburg/pkg/types"
)

// NotificationRouter routes any event payloads coming from Brandenburg API
type NotificationRouter struct {
	RequestCreatedHandler   shared.NotificationHandler
	ApprovalReceivedHandler shared.NotificationHandler
	AccessChangedHandler    shared.NotificationHandler
}

// Process routes incoming Brandenburg notifications
func (n *NotificationRouter) Process(w http.ResponseWriter, r *http.Request) {

	var notification *types.Notification
	body, err := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &notification)

	if err != nil {
		log.WithField("payload", body).Warn("Unable to parse POST body")
		n.sendError(400, err.Error(), w)
		return
	}

	// Route to the right handler depending on notification Type
	var responseJSON string
	switch notification.Type {
	case types.NotificationTypes.RequestCreated:
		responseJSON, err = n.RequestCreatedHandler.ProcessNotification(notification)
	case types.NotificationTypes.ApprovalReceived:
		responseJSON, err = n.ApprovalReceivedHandler.ProcessNotification(notification)
	case types.NotificationTypes.AccessChanged:
		responseJSON, err = n.AccessChangedHandler.ProcessNotification(notification)
	}

	// Handle any errors from handler
	if err != nil {
		n.sendError(500, err.Error(), w)
		return
	}
	// Send JSON reply
	w.Write([]byte(responseJSON))
}

func (n *NotificationRouter) sendError(status int, message string, w http.ResponseWriter) {
	type ErrorResponse struct {
		StatusCode   int    `json:"status"`
		ErrorMessage string `json:"message"`
	}

	response := ErrorResponse{
		StatusCode:   status,
		ErrorMessage: message,
	}
	payload, _ := json.Marshal(&response)
	w.WriteHeader(response.StatusCode)
	w.Write(payload)
}
