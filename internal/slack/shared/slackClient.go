package shared

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
	slackAPI "github.com/slack-go/slack"
	"gitlab.com/bradleyscott/brandenburg/internal/slack/payloads"
)

// Slacker describes a type that can talk to the Slack API
type Slacker interface {
	GetUserID(email string) (string, error)
	GetUsername(email string) (string, error)
	GetUsernames(emails []string) ([]string, error)
	GetEmail(userID string) (string, error)
	PostMessage(channel string, blocks string, text string) error
	ChatUpdate(channel string, timestamp string, blocks string, text string) error
	OpenConversation(recipientEmails []string) (string, error)
	OpenView(view string) error
}

// Client can talk to the Slack API
type Client struct {
	Token string
	API   *slackAPI.Client
}

// NewClient creates a new Slack Client using the provided token
func NewClient(token string) Client {
	if token == "" {
		log.WithField("token", token).Panic("A Slack token is required")
	}

	api := slackAPI.New(token)
	client := Client{Token: token, API: api}
	return client
}

// GetUserID returns a Slack ID that matches a provided email
func (c Client) GetUserID(email string) (string, error) {
	user, err := c.API.GetUserByEmail(email)
	if err != nil {
		log.WithFields(log.Fields{
			"email": email,
			"err":   err.Error(),
		}).Error("Unable to retrieve Slack user associated with email")
		return "", err
	}

	return user.ID, nil
}

// GetUsernames returns a Slack username for each matching email address in the emails slice
func (c Client) GetUsernames(emails []string) ([]string, error) {
	usernames := make([]string, len(emails))
	for i, email := range emails {
		username, err := c.GetUsername(email)
		if err != nil {
			return usernames, err
		}
		usernames[i] = username
	}
	return usernames, nil
}

// GetUsername returns a Slack username that matches a provided email
func (c Client) GetUsername(email string) (string, error) {
	user, err := c.API.GetUserByEmail(email)
	if err != nil {
		log.WithFields(log.Fields{
			"email": email,
			"err":   err.Error(),
		}).Error("Unable to retrieve Slack user associated with email")
		return "", err
	}

	return user.Name, nil
}

// GetEmail returns the email address associated with a given Slack userID
func (c Client) GetEmail(userID string) (string, error) {
	user, err := c.API.GetUserInfo(userID)
	if err != nil {
		log.WithFields(log.Fields{
			"userID": userID,
			"err":    err,
		}).Error("Unable to retrieve Slack user associated this userID")
		return "", err
	}

	return user.Profile.Email, nil
}

// PostMessage sends a message to a pre-opened channel
func (c Client) PostMessage(channel string, blocks string, text string) error {
	payload := payloads.PostMessageRequest{
		Text:    text,
		Channel: channel,
		Blocks:  blocks,
	}

	var response payloads.PostMessageResponse
	return c.postToSlack("chat.postMessage", payload, &response)
}

// ChatUpdate edits a previously sent message
func (c Client) ChatUpdate(channel string, timestamp string, blocks string, text string) error {
	payload := payloads.ChatUpdateRequest{
		Text:      text,
		Timestamp: timestamp,
		Channel:   channel,
		Blocks:    blocks,
	}

	var response payloads.ChatUpdateResponse
	return c.postToSlack("chat.update", payload, &response)
}

// OpenConversation opens a new conversation with the provided recipients
func (c Client) OpenConversation(recipientEmails []string) (string, error) {
	slackUsers := make([]string, len(recipientEmails))
	for i, email := range recipientEmails {
		slackUser, _ := c.GetUserID(email)
		slackUsers[i] = slackUser
	}

	payload := payloads.OpenConversationRequest{
		Users: strings.Join(slackUsers, ","),
	}

	var response payloads.OpenConversationResponse
	err := c.postToSlack("conversations.open", payload, &response)
	return response.Channel.ID, err
}

// OpenView opens a Slack modal
func (c Client) OpenView(view string) error {
	return c.postJSONToSlack("views.open", view, nil)
}

func (c *Client) postToSlack(path string, payload interface{}, target interface{}) error {
	payloadJSON, err := json.Marshal(payload)
	if err != nil {
		log.WithField("err", err.Error()).Error("Unable to marshal payload")
		return err
	}
	return c.postJSONToSlack(path, string(payloadJSON), &target)
}

func (c Client) postJSONToSlack(path string, payload string, target interface{}) error {
	url := "https://slack.com/api/" + path

	body := bytes.NewBuffer([]byte(payload))
	request, _ := http.NewRequest("POST", url, body)
	request.Header.Set("Authorization", "Bearer "+c.Token)
	request.Header.Set("Content-Type", "application/json")

	client := http.Client{}
	resp, err := client.Do(request)

	if err != nil {
		log.WithFields(log.Fields{
			"path":    path,
			"payload": payload,
			"err":     err.Error(),
		}).Error("A problem occurred when sending request to Slack")
		return err
	}
	defer resp.Body.Close()

	responseBody, _ := ioutil.ReadAll(resp.Body)
	var slackResponse payloads.SlackResponse
	err = json.Unmarshal(responseBody, &slackResponse)
	if err != nil {
		log.WithFields(log.Fields{
			"body": string(responseBody),
			"err":  err.Error(),
		}).Error("A problem occurred when trying to understand Slack's response body")
		return err
	} else if slackResponse.Ok == false {
		message := "Slack responded with an error code"
		log.WithField("body", string(responseBody)).Error(message)
		return errors.New(message)
	}

	err = json.Unmarshal(responseBody, &target)
	if err != nil {
		log.WithFields(log.Fields{
			"body": string(responseBody),
			"err":  err.Error(),
		}).Error("A problem occurred when trying to unmarshal response to target")
		return err
	}

	return nil
}
