package types

import (
	"time"
)

// AccessChange describes when and by who system access was provisioned
type AccessChange struct {
	Owner     string    `json:"owner" bson:"owner"`
	Type      string    `json:"type" bson:"type"`
	ChangedAt time.Time `json:"changedAt" bson:"changedAt"`
}

// IsValid indicates whether the AccessChange properties are complete and valid
func (a *AccessChange) IsValid() bool {
	isChangeTypeValid := (a.Type == AccessChangeTypes.Granted || a.Type == AccessChangeTypes.Declined || a.Type == AccessChangeTypes.Removed)
	isValid := (isChangeTypeValid && a.Owner != "" && !a.ChangedAt.IsZero())
	return isValid
}

const (
	accessGranted  = "ACCESS_GRANTED"
	accessDeclined = "ACCESS_DECLINED"
	accessRemoved  = "ACCESS_REMOVED"
)

type accessChangeType struct {
	Granted  string
	Declined string
	Removed  string
}

// AccessChangeTypes describes whether access was granted, declined or removed
var AccessChangeTypes = accessChangeType{
	Granted:  accessGranted,
	Declined: accessDeclined,
	Removed:  accessRemoved,
}
