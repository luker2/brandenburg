package types

import (
	"time"
)

// Approval records when and who approved (or declined) an access request
type Approval struct {
	Approver   string    `json:"approver" bson:"approver"`
	Type       string    `json:"type" bson:"type"`
	ReceivedAt time.Time `json:"receivedAt" bson:"receivedAt"`
}

// IsValid indicates whether the Approval properties are complete and valid
func (a *Approval) IsValid() bool {
	isValidType := (a.Type == ApprovalTypes.Approved || a.Type == ApprovalTypes.Declined)
	isValid := (isValidType && a.Approver != "" && !a.ReceivedAt.IsZero())
	return isValid
}

const (
	approved = "APPROVED"
	declined = "DECLINED"
)

type approvalType struct {
	Approved string
	Declined string
}

// ApprovalTypes describes whether approval was granted or declined
var ApprovalTypes = approvalType{
	Approved: approved,
	Declined: declined,
}
