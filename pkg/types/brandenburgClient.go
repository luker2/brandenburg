package types

// BrandenburgClient describes a type that can talk to the API
type BrandenburgClient interface {
	GetAccessRequest(id string) (*AccessRequest, error)
	CreateAccessRequest(proposed *AccessRequest) (*AccessRequest, error)
	RecordAccessChange(change *AccessChange, accessRequestID string) (*AccessRequest, error)
	RecordApproval(approval *Approval, accessRequestID string) (*AccessRequest, error)
	GetApplications() ([]*Application, error)
	GetApplication(id string) (*Application, error)
	CreateApplication(proposed *Application) (*Application, error)
}
